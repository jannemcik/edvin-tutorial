import { ServiceCollection } from "https://deno.land/x/di/mod.ts";
import { UserModel } from "/db/user.ts";
import { Mongo } from "/db/mongo.ts";
import { ServiceIdent } from "https://deno.land/x/di@v0.1.1/service.ts";
import { AuthService } from "/auth/auth.service.ts";
import { RecipeModel } from "/db/recipe.ts";
import { RecipeService } from "/services/recipe.service.ts";

export class IoCContainer {
  static serviceCollection = new ServiceCollection();

  static getInstance<T>(ident: ServiceIdent<T>): T {
    return IoCContainer.serviceCollection.get(ident);
  }

  static loadDeps() {
    IoCContainer.serviceCollection.addSingleton(Mongo);
    IoCContainer.serviceCollection.addScoped(UserModel);
    IoCContainer.serviceCollection.addScoped(AuthService);
    IoCContainer.serviceCollection.addScoped(RecipeModel);
    IoCContainer.serviceCollection.addScoped(RecipeService)
  }
}
