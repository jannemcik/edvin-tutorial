import { Bson } from "https://deno.land/x/mongo@v0.22.0/mod.ts";
import { Collection } from "https://deno.land/x/mongo@v0.22.0/src/collection/mod.ts";
import { Database } from "https://deno.land/x/mongo@v0.22.0/src/database.ts";
import { IoCContainer } from "/ioc.definitions.ts";
import { Mongo } from "/db/mongo.ts";
import { RecipeSchema } from "/schema/recipe.ts";
import { Service } from "https://deno.land/x/di@v0.1.1/mod.ts";

@Service()
export class RecipeModel {
  #db: Database;
  #recipeCollection: Collection<RecipeSchema>;

  constructor() {
    this.#db = IoCContainer.getInstance(Mongo).db;
    this.#recipeCollection = this.#db.collection("recipe");
  }

  findRecipe(id: string) {
    return this.#recipeCollection.findOne({ _id: new Bson.ObjectID(id) });
  }

  findUserRecipes(id: string) {
    const bsonId = new Bson.ObjectId(id);
    return this.#recipeCollection.find({ authorId: bsonId }).toArray();
  }

  getAllRecipes() {
    return this.#recipeCollection.find({ shared: true }).sort({postedDate: 1}).toArray();
  }

  insertRecipe(recipe: RecipeSchema) {
    return this.#recipeCollection.insertOne({...recipe, postedDate: new Date(), authorId: new Bson.ObjectID(recipe.authorId)})
  }

  removeRecipe(id: string) {
    return this.#recipeCollection.deleteOne({_id: new Bson.ObjectID(id)})
  }

  updateRecipe(id: string, updateData: Partial<RecipeSchema>) {
    return this.#recipeCollection.updateOne({_id: new Bson.ObjectID(id)}, { $set: updateData})
  }
}
