import { Service } from "https://deno.land/x/di/mod.ts";
import { Collection } from "https://deno.land/x/mongo@v0.22.0/src/collection/mod.ts";
import { Database } from "https://deno.land/x/mongo@v0.22.0/src/database.ts";
import { IoCContainer } from "/ioc.definitions.ts";
import { Mongo } from "/db/mongo.ts";
import { UserSchema } from "/schema/user.ts";
import { Bson } from "https://deno.land/x/mongo@v0.22.0/mod.ts";

@Service()
export class UserModel {
  #db: Database;
  #userCollection: Collection<UserSchema>;

  constructor() {
    const mongo = IoCContainer.getInstance(Mongo);
    this.#db = mongo.db;
    this.#userCollection = this.#db.collection("user");
  }

  findUser(email: string) {
    const options: any = {
      projection: { password: 0, salt: 0 },
      noCursorTimeout: false
    }
    return this.#userCollection.findOne({ email }, options);
  }

  findUserForLogin(email: string) {
    return this.#userCollection.findOne({ email });
  }

  createUser(email: string, password: string, salt: string) {
    return this.#userCollection.insertOne({ email, password, salt, registrationDate: new Date() });
  }

  findUserById(id: string) {
    return this.#userCollection.findOne({ _id: new Bson.ObjectID(id) }, {
      projection: { password: 0, salt: 0 },
    });
  }

  setToken(id: string, token: string) {
    return this.#userCollection.updateOne({ _id: new Bson.ObjectID(id) }, { $set: {token, lastLogin: new Date()} })
  }

  updateUser(id: string, updateData: Partial<UserSchema>) {
    return this.#userCollection.updateOne({ _id: new Bson.ObjectID(id) }, { $set: updateData})
  }

  unsetToken(email: string) {
    return this.#userCollection.updateOne({ email}, { $unset: { token: ''} })
  }
}
