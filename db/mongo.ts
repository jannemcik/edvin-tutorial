import { MongoClient } from "https://deno.land/x/mongo@v0.22.0/mod.ts";
import { CONFIG } from "/config.ts";
import { Service } from "https://deno.land/x/di/mod.ts";
@Service()
export class Mongo {
  #client: MongoClient;

  get client() {
    return this.#client;
  }

  get db() {
    return this.client.database(CONFIG.MONGO_DB);
  }

  constructor() {
    this.#client = new MongoClient();
  }

  connect() {
    return this.#client.connect({
      db: CONFIG.MONGO_DB,
      servers: [{
        host: CONFIG.MONGO_HOST,
        port: 27017
      }],
      credential: {
        username: CONFIG.MONGO_USER,
        password: CONFIG.MONGO_PASSWORD,
        mechanism: 'SCRAM-SHA-1',
        db: CONFIG.MONGO_DB
      }
    }).then(() => console.log("Successfully connected to mongo"));
  }
}
