export interface UserSchema {
  _id: { $oid: string };
  email: string;
  password: string;
  salt: string;
  lastLogin: Date;
  token: string;
  isLoggedIn: string;
  firstName?: string;
  lastName?: string;
}
