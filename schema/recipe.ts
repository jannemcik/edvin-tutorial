export interface RecipeSchema {
  _id: { $oid: string };
  name: string;
  cover: string;
  portions: string;
  difficulty: string;
  cookingTime: string;
  ovenNeeded: boolean;
  ingredients: IngredientSchema[];
  foodType: string;
  kitchenType: string;
  description: string;
  postedDate: Date;
  shared: boolean;
  authorId: { $oid: string };
}

interface IngredientSchema {
  name: string;
  quantity: number;
  unit: string;
}
