import "https://cdn.pika.dev/@abraham/reflection@^0.7.0";
import { Application } from "https://deno.land/x/oak/mod.ts";
import { Mongo } from "/db/mongo.ts";
import { CONFIG } from "/config.ts";
import { router } from "/server/routes.ts";
import { IoCContainer } from "/ioc.definitions.ts";

IoCContainer.loadDeps();

const PORT = +(Deno.env.get('PORT') ?? CONFIG.PORT);
const app = new Application();
app.use(router.routes());
app.use(router.allowedMethods());
const mongo = IoCContainer.serviceCollection.get(Mongo);

await mongo.connect();
app.addEventListener("listen", () => {
  console.log(
    `HTTP webserver running.  Access it at port: ${PORT}`,
  );
});

await app.listen({ port: PORT, hostname: '0.0.0.0' });
