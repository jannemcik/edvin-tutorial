FROM hayd/alpine-deno:1.9.0

# The port that your application listens to.
WORKDIR /app

# Prefer not to run as root.
USER deno

# Cache the dependencies as a layer (the following two steps are re-run only when deps.ts is modified).
# Ideally cache deps.ts will download and compile _all_ external files used in main.ts.
COPY . .
RUN deno cache ./*.ts --import-map=import_map.json --unstable --reload

COPY docker/.env ./
# These steps will be re-run upon each file change in your working directory:
# ADD . .
# Compile the main app so that it doesn't need to be compiled each startup/entry.
RUN deno cache --import-map=import_map.json --unstable server.ts

CMD ["run", "--import-map=import_map.json", "--allow-read", "--allow-net", "--allow-env", "--unstable", "server.ts"]
