import { Router, RouterContext } from "https://deno.land/x/oak/mod.ts";
import { IoCContainer } from "/ioc.definitions.ts";
import { AuthService, decodeToken } from "/auth/auth.service.ts";
import { createHttpError } from "https://deno.land/x/oak/httpError.ts";
import { RecipeService } from "/services/recipe.service.ts";
import { getQuery } from "https://deno.land/x/oak/helpers.ts";

function extractBearerToken(headers: Headers): string {
  const authHeaders = headers.get("Authorization");
  if (authHeaders?.startsWith("Bearer ")) {
    return authHeaders.substring(7, authHeaders.length);
  }
  return "";
}
const validateJwtMiddleware = async (ctx: RouterContext, next: () => Promise<any>) => {
  const headers = await ctx.request.headers;
  const bearer = extractBearerToken(headers);
  if (!bearer) {
    throw createHttpError(401, "Unauthorized");
  }
  try {
    const authService = IoCContainer.getInstance(AuthService)
    const isValid = await authService.validateToken(bearer);
    if (!isValid) {
      throw new Error('Invalid token')
    }
  } catch (error) {
    console.error(error);
    throw createHttpError(401, "Unauthorized");
  }
  return next()
};

const router = new Router();

router.get("/recipes", validateJwtMiddleware, async (ctx) => {
  try {
    const recipeService = IoCContainer.getInstance(RecipeService)
    const recipes = await recipeService.getRecipes()
    ctx.response.body = recipes
    ctx.response.type = "application/json"
  } catch (error) {
    console.error(error)
  }
});

router.get("/recipe/:id", async (ctx) => {
  try {
    const recipeId =  getQuery(ctx, { mergeParams: true }).id
    const recipeService = IoCContainer.getInstance(RecipeService)
    const recipes = await recipeService.getRecipe(recipeId)
    ctx.response.body = recipes
  } catch (error) {
    console.error(error)
  }
});

router.post("/recipe/:id", async (ctx) => {
  try {
    const recipeId =  getQuery(ctx, { mergeParams: true }).id
    const body = await ctx.request.body({ type: "json" });
    const recipe = await body.value;
    const recipeService = IoCContainer.getInstance(RecipeService)
    const updated = await recipeService.updateRecipe(recipeId, recipe)
    ctx.response.body = updated
  } catch (error) {
    console.error(error)
  }
});
router.post("/recipe", async (ctx) => {
  try {
    const body = await ctx.request.body({ type: "json" });
    const recipe = await body.value;
    const recipeService = IoCContainer.getInstance(RecipeService)
    const inserted = await recipeService.insertRecipe(recipe)
    ctx.response.body = inserted
  } catch (error) {
    console.error(error)
  }
});


router.delete("/recipe/:id", async (ctx) => {
  try {
    const recipeId =  getQuery(ctx, { mergeParams: true }).id
    const recipeService = IoCContainer.getInstance(RecipeService)
    await recipeService.removeRecipe(recipeId)
    ctx.response.status = 204
  } catch (error) {
    console.error(error)
  }
});

router.post("/auth/login", async (ctx) => {
  try {
    const body = await ctx.request.body({ type: "json" });
    const { email, password } = await body.value;
    const authService = IoCContainer.getInstance(AuthService);
    const user = await authService.login(email, password);
    if (!user) {
      throw createHttpError()
    }
    ctx.response.body = user
  } catch (error) {
    console.error(error);
    throw error;
  }
});
router.post("/auth/register", async (ctx) => {
  try {
    const body = await ctx.request.body({ type: "json" });
    const { email, password } = await body.value;
    const authService = IoCContainer.getInstance(AuthService);
    const userId: any = await authService.register(email, password);
    const newUser = await authService.retrieveUser(userId);
    ctx.response.body = newUser;
  } catch (error) {
    console.error(error);
    throw error;
  }
});

router.delete("/auth/logout", validateJwtMiddleware, async (ctx) => {
  try {
    const headers = await ctx.request.headers
    const token = extractBearerToken(headers)
    const [_, {sub: email}] = decodeToken(token)
    const authService = IoCContainer.getInstance(AuthService);
    await authService.logout(email);
    ctx.response.status = 204
  } catch (error) {
    console.error(error);
    throw error;
  }
});

router.get("/profile", validateJwtMiddleware,  async (ctx) => {
  try {
    const headers = await ctx.request.headers
    const token = extractBearerToken(headers)
    const [_, {sub: email}] = decodeToken(token)
    const authService = IoCContainer.getInstance(AuthService);
    const user = await authService.retrieveUserByEmail(email)
    ctx.response.body = user
  } catch (error) {
    console.error(error);
    throw error;
  }
});

export { router };
