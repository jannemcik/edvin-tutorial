import { IoCContainer } from "/ioc.definitions.ts";
import { RecipeModel } from "/db/recipe.ts";
import { Service } from "https://deno.land/x/di@v0.1.1/mod.ts";
import { RecipeSchema } from "/schema/recipe.ts";

@Service()
export class RecipeService {
  #recipeModel: RecipeModel;
  constructor() {
    this.#recipeModel = IoCContainer.getInstance(RecipeModel);
  }

  getRecipes() {
      return this.#recipeModel.getAllRecipes()
  }

  getRecipe(id: string) {
      return this.#recipeModel.findRecipe(id)
  }

  async insertRecipe(recipe: RecipeSchema) {
    const recipeId = await this.#recipeModel.insertRecipe(recipe)
    return this.getRecipe(recipeId as unknown as string)
  }

  removeRecipe(id: string) {
    return this.#recipeModel.removeRecipe(id)
  }

  async updateRecipe(id: string, recipe: Partial<RecipeSchema>) {
    await this.#recipeModel.updateRecipe(id, recipe)
    return this.getRecipe(id)
  }
}