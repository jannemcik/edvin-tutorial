import { create, getNumericDate, verify, decode } from "https://deno.land/x/djwt@v2.2/mod.ts";
import * as bcrypt from "https://deno.land/x/bcrypt/mod.ts";
import { CONFIG } from "/config.ts";
import { default as Random } from "https://deno.land/x/random@v1.1.2/Random.js";
import { UserModel } from "/db/user.ts";
import { Inject, Service } from "https://deno.land/x/di/mod.ts";
import { IoCContainer } from "/ioc.definitions.ts";
import { createHttpError } from "https://deno.land/x/oak/httpError.ts";
import type { UserSchema } from "/schema/user.ts";

export const generateJWT = async (userId: string, email: string) =>
  await create({ alg: "HS512", typ: "JWT" }, {
    iss: userId,
    sub: email,
    exp: getNumericDate(60 * 60 * 24 * 7),
    iat: getNumericDate(new Date()),
  }, CONFIG.SECRET);

  export const validateToken = (token:string) => {
    return verify(token, CONFIG.SECRET, 'HS512');
  }

export const decodeToken = (token: string) => decode(token) as [unknown, {sub: string}, unknown]


@Service()
export class AuthService {
  #randomizer: Random;
  #userModel: UserModel;
  constructor() {
    this.#randomizer = new Random();
    this.#userModel = IoCContainer.getInstance(UserModel);
  }

  private async encryptPassword(password: string) {
    const salt = await bcrypt.genSalt(this.#randomizer.int(10, 15));
    const hash = await bcrypt.hash(password, salt);
    return [hash, salt];
  }

  private async decryptPassword(password: string, salt: string) {
    const hash = await bcrypt.hash(password, salt);
    return hash;
  }

  private async checkTokenValidityOrGenerate(user: UserSchema) {
    let token = ''
    if (user.token) {
      console.info('Using existing token')
      try {
        await validateToken(user.token)
        token = user.token
      } catch (error) {
        console.error(error)
      }
    }
    if (!token) {
      token = await generateJWT(user._id.toString(), user.email);
    }

    return token;
  }

  public async validateToken(token: string) {
    const [_, {sub: email}] = decodeToken(token)
    const user = await this.retrieveUserByEmail(email)
    let t = ''
    if (user?.token) {
      console.info('Using existing token')
      try {
        await validateToken(user.token)
        t = user.token
      } catch (error) {
        console.error(error)
      }
    }
    return !!t
  }

  async register(email: string, password: string) {
    const userExists = await this.#userModel.findUser(email);
    if (userExists) {
      throw createHttpError(409, "User already exists");
    }
    const [hash, salt] = await this.encryptPassword(password);
    return this.#userModel.createUser(email, hash, salt);
  }

  async login(email: string, password: string) {
    const user = await this.#userModel.findUserForLogin(email);
    if (user) {
      const hash = await this.decryptPassword(password, user.salt);
      if (hash === user.password) {
        const userId = user._id.toString()
        const token = await this.checkTokenValidityOrGenerate(user)
        await this.#userModel.setToken(userId, token)
        return this.retrieveUser(userId);
      } else {
        throw createHttpError(401, "Unauthorized");
      }
    } else {
      throw createHttpError(404, "User not found");
    }
  }

  retrieveUser(id: string){
    return this.#userModel.findUserById(id) as Promise<{_id: string, email: string, token?: string} | undefined>
  }

  retrieveUserByEmail(email: string) {
    return this.#userModel.findUser(email) as Promise<{_id: string, email: string, token?: string} | undefined>

  }

  logout(email: string) {
    return this.#userModel.unsetToken(email)
  }
}
